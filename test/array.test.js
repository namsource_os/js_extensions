require("../arrayExtensions");

describe('#Array Extensions - Test array extensions', function () {

    beforeEach(function () { })

    let people = [
        { first: "mike", last: "smith" },
        { first: "nick", last: "jones" },
        { first: "nicole", last: "picket" },
        { first: "emily", last: "wilson" },
        { first: "rick", last: "wilson" }
    ];

    it('array.Where - should find mike', function () {

        people.Where(p => p.first === "mike").First().first.should.be.eql("mike");

    });

    it('array.Last - should find rick', function () {

        people.Where(p => p.last === "wilson").Last().first.should.be.eql("rick");

    });


    it('array.IndexOf - should find correct index of nick', function () {

        let nick = people.Where(p => p.first === "nick").First();
        let index = people.IndexOf(nick);
        index.should.be.eql(1);

    });

    it('array.IndexWhere - should find index of nick by name', function () {

        let index = people.IndexWhere(p => p.first === "nick");
        index.should.be.eql(1);

    });


    it('array.Contain = shouldbe true for nick', function () {

        let nick = people.Where(p => p.first === "nick").First();
        people.Contains(nick).should.be.eql(true);

    });

    it('array.Insert - should insert betty at index 2', function () {

        let betty = { first: "betty", last: "tyson" };
        people.Insert(betty, 2);
        people.IndexOf(betty).should.be.eql(2);
        people.IndexWhere(p => p.first === "nicole").should.be.eql(3);

    });

    it('array.Append = should add roger', function () {

        let roger = { first: "roger", last: "moore" };
        people.Append(roger);
        people.IndexOf(roger).should.be.eql(people.length - 1);

    });

    it('array.AppendIfMissing - should not append tim twice', function () {

        let tim = { first: "tim", last: "rice" };
        people.Append(tim);
        people.AppendIfMissing(tim);
        people.Where(p => p.first === "tim").length.should.be.eql(1);

    });

    it('array.Remove - should remove anne', function () {

        let anne = { first: "anne", last: "frank" };
        people.Append(anne);
        people.IndexWhere(p => p.first === "anne").should.not.be.eql(-1);
        people.Remove(anne);
        people.IndexWhere(p => p.first === "anne").should.be.eql(-1);

    });

    it('array.RemoveAtIndex - should remove james', function () {

        let james = { first: "james", last: "cooke" };
        people.Insert(james, 0);
        people.IndexWhere(p => p.first === "james").should.be.eql(0);
        people.RemoveAtIndex(0);
        people.IndexWhere(p => p.first === "james").should.be.eql(-1);

    });


    it('array.Bump - should move colin up and down in the array', function () {

        let colin = { first: "colin", last: "wormald" };
        people.Insert(colin, 0);
        let index = people.IndexWhere(p => p.first === "colin");

        people.BumpItem(colin, 1);
        people.IndexWhere(p => p.first === "colin").should.be.eql(index + 1);

        people.BumpItem(colin, -1);
        people.IndexWhere(p => p.first === "colin").should.be.eql(index);

    });

});