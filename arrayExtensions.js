// These methods have been created to support simple method calls to manipulate arrays

Array.prototype.Where = function (func) {
    if (!func) {
        throw new Error("func is required")
    }

    return this.filter(i => func(i));
}

Array.prototype.First = function () {
    if (this.length == 0)
        return null;

    return this[0];
}

Array.prototype.Last = function () {
    if (this.length == 0)
        return null;

    return this[this.length - 1];
}

Array.prototype.IndexOf = function (obj) {
    if (!obj) {
        throw new Error("obj is required")
    }

    return this.findIndex(i => i == obj);
}


Array.prototype.IndexWhere = function (func) {
    if (!func) {
        throw new Error("func is required")
    }

    var obj = this.Where(func).First();
    return obj ? this.indexOf(obj) : -1;
}

Array.prototype.Contains = function (obj) {
    if (!obj) {
        throw new Error("obj is required")
    }

    return this.IndexOf(obj) != -1;
}

Array.prototype.Insert = function (obj, index) {
    if (!obj) {
        throw new Error("obj is required")
    }

    if (index===null) {
        throw new Error("index is required")
    }

    this.splice(index, 0, obj);
}

Array.prototype.Append = function (obj) {
    if (!obj) {
        throw new Error("obj is required")
    }

    this.push(obj);
}

Array.prototype.AppendIfMissing = function (obj) {
    if (!obj) {
        throw new Error("obj is required")
    }

    const index = this.findIndex(i => i == obj);
    if (index == -1) {
        this.Append(obj);
    }
}

Array.prototype.Remove = function (obj) {
    if (!obj) {
        throw new Error("obj is required")
    }

    const index = this.IndexOf(obj);
    if (index != -1)
        this.splice(index, 1);
}

Array.prototype.RemoveAtIndex = function (index) {
    if (index===null) {
        throw new Error("index is required")
    }

    this.splice(index, 1);
}

Array.prototype.Replace = function (obj, replacement) {
    if (!obj) {
        throw new Error("obj is required")
    }

    if (!replacement) {
        throw new Error("obj is required")
    }

    this.Remove(obj);
    this.Append(replacement);
}

Array.prototype.BumpItem = function (obj, offset) {
    if (!obj) {
        throw new Error("obj is required")
    }

    if (!offset) {
        throw new Error("offset is required")
    }

    if (this.length == 0)
        throw new Error("empty array");

    if (offset != 1 && offset != -1)
        throw new Error("offset must be either 1 or -1");

    var index = this.IndexOf(obj);

    if (index == -1
        || (index <= 0 && offset == -1)
        || (index >= this.length - 1 && offset == 1)
    ) {
        throw new Error("index out of bounds");
    }

    this.Remove(obj);
    this.Insert(obj, index + offset);
}
